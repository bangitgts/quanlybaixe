const EmployeeModel = require("../models/Employee");

module.exports = async (req, res, next) => {
  try {
    const user = await EmployeeModel.findOne({ _id: req.user._id });
    if (user.role === "employee") {
      next();
    }
  } catch (error) {
    res.status(402).send({
      status: 402,
      success: false,
      message: "Login with wrong authorization",
    });
  }
};
