const accountRouter = require("./account.route");
const vehileRouter = require("./vehicle.route.js");
const capacityRouter = require("./capacity.route")
function route(app) {
  app.use("/Account", accountRouter);
  app.use("/Vehicle", vehileRouter);
  app.use("/Capacity", capacityRouter);
}

module.exports = route;
