const express = require("express");
const router = express.Router();
const vehicleController = require("../controllers/VehicleController");
const manageControllers = require("../controllers/ManageController");

router.post("/DangKyXe/:employeeId", vehicleController.registerVehicle);
router.get("/GetDanhSachXe", vehicleController.getVehicleWidthTypeVehicle);
router.get("/GetThongTinXe", vehicleController.getThongTinXe);
router.get("/GetXeCuaChuXe/:employeeId", vehicleController.getXeCuaEmployee);
router.get("/LayDanhSachTrangThaiXe/:employeeId", vehicleController.getVehicle);
router.post("/CheckInXeVaoBai", vehicleController.checkInVehicle);
router.post("/CheckOutXeTrongBai", vehicleController.checkOutVehicle);
router.post("/CheckOutXeWithId", vehicleController.checkOutForAdmin);
router.get("/GetTimeCheckInCheckOut", manageControllers.getInForMation);
router.get("/GetTimeCheckIn", manageControllers.getInForMationInTime);
router.get("/GetTimeCheckOut", manageControllers.getInForMationOutTime);
router.delete("/DeleteVehicleById", vehicleController.deleteVehicle);
router.get("/GetVehicleInParking", vehicleController.getVehicleInParking)
router.get("/GetAllXe", vehicleController.getDanhSachXe)
module.exports = router;
