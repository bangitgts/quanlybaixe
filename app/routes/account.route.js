const express = require("express");
const router = express.Router();
const checkToken = require("../auth/CheckToken");
const employeeController = require("../controllers/EmployeeController");

router.post("/Login",employeeController.loginAccount);
router.post("/Register", employeeController.registerUser);
router.post("/ChangePassword", checkToken, employeeController.changePassword);
router.put("/EditInforMation", employeeController.editInformation);
router.get("/GetEmployee", employeeController.getEmployee);
router.post("/ResetPassword", employeeController.resetPassword);
router.get("/ResetPassword", employeeController.resetPassword);
router.delete("/DeleteById", employeeController.deleteEmployee);


module.exports = router;
