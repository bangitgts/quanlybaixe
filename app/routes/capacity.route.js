const express = require("express");
const router = express.Router();
const checkToken = require("../auth/CheckToken");
const capacityController = require("../controllers/CapacityControllers");

router.get("/GetNameCapacity", capacityController.gettAllTypeCapicity);
router.post("/Create", capacityController.create);
router.put("/ChangeCapacity", capacityController.changeCapacity);
router.get("/ShowInforCapacity", capacityController.showInforCapacity);

module.exports = router;
