const Pagination = (data, page_size, page_number) => {
  const start = (page_number - 1) * page_size;
  const end = page_size * page_number;
  return data.slice(start, end);
};
module.exports = Pagination;
