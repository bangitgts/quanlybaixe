const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var mongooseDelete = require("mongoose-delete");
const AutoIncrement = require("mongoose-sequence")(mongoose);
const ManageNotificationSchema = new Schema(
  {
    _id: {
      type: Number,
    },
    employeeId: {
      type: Number,
    },
    nameEmployee: {
      type: String,
    },
    typeVehicle: {
      type: String,
    },
    licensePlate: {
      type: String,
    },
    codeIn: {
      type: String,
    },
    dateIn: {
      type: Date,
    },
    dateOut: {
      type: Date,
      default: null,
    },
  },
  {
    _id: false,
    collection: "ManageNotification",
  }
);

// Add plugin
ManageNotificationSchema.plugin(AutoIncrement, {
  id: "ManageNotification",
  inc_field: "_id",
});
ManageNotificationSchema.plugin(mongooseDelete, {
  deleteAt: true,
  overrideMethods: "all",
});
module.exports = mongoose.model("ManageNotification", ManageNotificationSchema);
