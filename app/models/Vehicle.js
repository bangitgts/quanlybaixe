const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var mongooseDelete = require("mongoose-delete");
const AutoIncrement = require("mongoose-sequence")(mongoose);
const VehicleSchema = new Schema(
  {
    _id: {
      type: Number,
    },
    employeeId: {
      type: Number,
    },
    nameEmployee: {
      type: String,
    },
    typeVehicle: {
      type: String,
    },
    licensePlate: {
      type: String,
    },
    codeIn: {
      type: String,
      default: null,
    },
    qr: {
      type: String,
      default: null,
    },
    createDate: {
      type: Date,
    },
  },
  {
    _id: false,
    collection: "Vehicle",
  }
);

// Add plugin
VehicleSchema.plugin(AutoIncrement, { id: "Vehicle", inc_field: "_id" });
VehicleSchema.plugin(mongooseDelete, {
  deleteAt: true,
  overrideMethods: "all",
});
module.exports = mongoose.model("Vehicle", VehicleSchema);
