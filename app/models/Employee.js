const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var mongooseDelete = require("mongoose-delete");
const AutoIncrement = require("mongoose-sequence")(mongoose);
const EmployeeSchema = new Schema(
  {
    _id: {
      type: Number,
    },
    fullName: {
      type: String,
      minLength: 1,
      required: [true, "fullName required"],
    },
    role: {
      type: String,
      default: "employee",
    },
    username: {
      type: String,
      lowercase: true,
      unique: true,
      required: [true, "Username required"],
    },
    password: {
      type: String,
      required: [true, "Password required"],
    },
    gender: {
      type: Number, // 0 la nu, 1 la nam
      require: true,
    },
    createDate: {
      type: Date,
    },
  },
  {
    _id: false,
    collection: "Employee",
  }
);

// Add plugin
EmployeeSchema.plugin(AutoIncrement, {id: 'Employee', inc_field: '_id'});
EmployeeSchema.plugin(mongooseDelete, {deleteAt: true,overrideMethods: "all",});
module.exports = mongoose.model("Employee", EmployeeSchema);
