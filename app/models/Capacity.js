const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var mongooseDelete = require("mongoose-delete");
const AutoIncrement = require("mongoose-sequence")(mongoose);
const CapacitySchema = new Schema(
  {
    _id: {
      type: Number,
    },
    typeVehicle: {
      type: String,
    },
    nowVehicle: {
      type: Number,
      default: 0,
    },
    capacity: {
      type: Number,
    },
  },
  {
    _id: false,
    collection: "Capacity",
  }
);

// Add plugin
CapacitySchema.plugin(AutoIncrement, { id: "Capacity", inc_field: "_id" });
CapacitySchema.plugin(mongooseDelete, {
  deleteAt: true,
  overrideMethods: "all",
});
module.exports = mongoose.model("Capacity", CapacitySchema);
