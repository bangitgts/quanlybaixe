const CapacityModel = require("../models/Capacity");

class CapacityController {
  async create(req, res) {
    const typeVehicle = req.query.typeVehicle;
    const capacity = req.query.capacity;
    CapacityModel.create({
      typeVehicle: typeVehicle,
      capacity: capacity,
    });
    res.status(200).json({
      status: 200,
      success: true,
      message: "Create successfully",
    });
  }

  async changeCapacity(req, res) {
    const _id = req.query._id;
    const capacity = req.query.capacity;
    const findCapacity = await CapacityModel.findOne({ _id: _id });
    try {
      if (findCapacity) {
        findCapacity.capacity = capacity;
        findCapacity.save();
        res.status(200).json({
          status: 200,
          success: true,
          message: "Change successfully",
        });
      }
    } catch (error) {
      res.status(500).json({
        status: 200,
        success: false,
        message: "Server Error",
      });
    }
  }

  async showInforCapacity(req, res) {
    const typeVehicle = req.query.typeVehicle;
    const findCapacity = await CapacityModel.findOne({ typeVehicle: typeVehicle });
    console.log(findCapacity);
    try {
      if (findCapacity) {
        let datas = {
          typeVehicle: typeVehicle,
          nowVehicle: findCapacity.nowVehicle,
          capacity: findCapacity.capacity,
        };

        res.status(200).json({
          status: 200,
          success: true,
          message: "Data Load successfully",
          data: datas,
        });
      }
    } catch (error) {
      res.status(500).json({
        status: 200,
        success: false,
        message: "Server Error",
      });
    }
  }

  async gettAllTypeCapicity(req, res) {
    const findCapacity = await CapacityModel.find({}).select("-__v");
    res.status(200).json({
      status: 200,
      success: true,
      message: "Data Load successfully",
      data: findCapacity,
    });
  }
}

module.exports = new CapacityController();
