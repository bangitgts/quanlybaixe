const VehicleModel = require("../models/Vehicle");
const EmployeeModel = require("../models/Employee");
const CapacityModel = require("../models/Capacity");

const ManageModel = require("../models/ManageNotification");
const md5 = require("md5");
const jwt = require("jsonwebtoken");
const Pagination = require("../utils/Pagination");
class EmployeeController {
  async loginAccount(req, res) {
    try {
      // req.headers['content-type'] = 'application/json';
      let username = req.body.username;
      let password = md5(req.body.password);
      const checkLogin = await EmployeeModel.findOne({
        username: username,
        password: password,
      }).select("-__v");

      if (checkLogin) {
        let token = jwt.sign(
          {
            exp: Math.floor(Date.now() / 1000) + 60 * 60,
            _id: checkLogin._id,
          },
          "password"
        );
        checkLogin.token = token;
        res.header("auth-token", token);

        return res.status(200).json({
          message: "Loggin successfully",
          data: Object.assign(checkLogin._doc, { token: token }),
          success: true,
          status: 200,
        });
      } else {
        return res.status(400).json({
          message: "Loggin failed. Account or password does not match",
          success: false,
          status: 400,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Server Error",
        success: false,
        status: 500,
      });
    }
  }

  async registerUser(req, res) {
    let fullName = req.body.fullName;
    let username = req.body.username;
    let password = md5(req.body.password);
    let role = req.body.role;
    let gender = parseInt(req.body.gender);
    try {
      console.log(fullName);
      if (gender === 0 || gender === 1) {
        const checkUsername = await EmployeeModel.findOne({
          username: username,
        });
        if (!checkUsername) {
          EmployeeModel.create({
            fullName: fullName,
            username: username,
            password: password,
            role: role,
            gender: gender,
          });
          return res.status(200).json({
            message: "Account created successfully",
            status: 200,
            success: true,
          });
        } else {
          res.status(402).json({
            status: 402,
            success: false,
            message: "This account has been created",
          });
        }
      } else {
        res.status(400).json({
          status: 400,
          success: false,
          message: "Input data gender is incorrect",
        });
      }
    } catch (error) {
      res.status(500).json({
        status: 500,
        success: false,
        err: error,
        message: "Register failed because of server error",
      });
    }
  }

  async changePassword(req, res) {
    let password = req.body.password;
    let newpassword = req.body.newpassword;
    try {
      const idUser = await EmployeeModel.findOne({ _id: req.user._id });
      if (md5(password) === idUser.password) {
        idUser.password = md5(newpassword);
        idUser.save();
        return res.status(200).json({
          message: "Password change successfully",
          success: true,
          status: 200,
        });
      } else {
        return res.status(402).json({
          message: "Old password is incorrect",
          success: false,
          status: 400,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Sever error",
        success: false,
        status: 500,
      });
    }
  }

  async resetPassword(req, res) {
    let idEmployee = req.query.idEmployee;
    const newpassword = "123456@@";
    try {
      const idUser = await EmployeeModel.findOne({ _id: idEmployee });
      if (idUser) {
        idUser.password = md5(newpassword);
        return res.status(200).json({
          message: "Đã reset mật khẩu thành công",
          success: true,
          status: 200,
        });
      } else {
        return res.status(402).json({
          message: "Không tìm thấy mã dân cư này",
          success: false,
          status: 400,
        });
      }
    } catch {
      return res.status(500).json({
        message: "Sever error",
        success: false,
        status: 500,
      });
    }
  }

  async editInformation(req, res) {
    const idEmployee = req.query.idEmployee;
    let fullName = req.body.fullName;
    let gender = parseInt(req.body.gender);
    try {
      if (gender === 0 || gender === 1) {
        const checkUsername = await EmployeeModel.findOne({
          _id: idEmployee,
        });
        const checkManage = await ManageModel.find({
          employeeId: idEmployee,
        });
        if (checkUsername) {
          checkUsername.fullName = fullName;
          checkUsername.gender = gender;
          checkUsername.save();
          for (let item of checkManage) {
            item.nameEmployee = fullName;
            item.save();
          }
          return res.status(200).json({
            message: "Đổi thông tin thành công",
            status: 200,
            success: true,
          });
        } else {
          res.status(402).json({
            status: 402,
            success: false,
            message: "Tài khoản này không hợp lệ",
          });
        }
      } else {
        res.status(400).json({
          status: 400,
          success: false,
          message: "Input data gender is incorrect",
        });
      }
    } catch (error) {
      res.status(500).json({
        status: 500,
        success: false,
        err: error,
        message: "Register failed because of server error",
      });
    }
  }

  async getEmployee(req, res) {
    try {
      let keySearchName = req.query.keySearchName;
      const page_size = parseInt(req.query.PageSize) || 500;
      const page_number = parseInt(req.query.PageIndex) || 1;
      let getData;
      if (keySearchName) {
        getData = await EmployeeModel.find({
          fullName: { $regex: keySearchName },
        }).select("-__v");
      } else {
        getData = await EmployeeModel.find({}).select("-__v");
      }
      res.status(200).json({
        success: true,
        status_code: 200,
        meta: {
          total: getData.length,
          page_size: page_size,
          page_number: page_number,
        },
        data: Pagination(getData.reverse(), page_size, page_number),
      });
    } catch (error) {
      return res.status(500).json({
        message: "Sever error",
        success: false,
        status: 500,
      });
    }
  }

  async deleteEmployee(req, res) {
    const employeeId = req.query.employeeId;
    try {
      const findEmployee = await EmployeeModel.findOne({ _id: employeeId });
      const findVehicle = await VehicleModel.findOne({ employeeId: employeeId });
      if (findVehicle) {
        res.status(400).json({
          success: false,
          status_code: 400,
          message: "Tài khoản này có xe trong hệ thống hãy xóa nó trước",
        });
      } else {
        if (findEmployee) {
          EmployeeModel.find({ _id: employeeId }).remove().exec();
          res.status(200).json({
            success: true,
            status_code: 200,
            message: "Xóa thành công",
          });
        } else {
          res.status(400).json({
            success: false,
            status_code: 400,
            message: "Không có tài khoản này trong hệ thống",
          });
        }
      }
    } catch (error) {
      res.status(500).json({
        success: false,
        status_code: 500,
        message: "Server Error",
      });
    }
  }
}

module.exports = new EmployeeController();
