const VehicleModel = require("../models/Vehicle");
const EmployeeModel = require("../models/Employee");
const CapacityModel = require("../models/Capacity");
const ManageNotificationModel = require("../models/ManageNotification");

const Pagination = require("../utils/Pagination");
const { formatDatePost, formatDatePostShort, formatDateNow } = require("../utils/ParseTimeDay");
const generateId = require("../utils/GenerateId");
const qrcode = require("qrcode");

class VehicleController {
  async registerVehicle(req, res) {
    try {
      const employeeId = req.params.employeeId;
      const typeVehicle = req.body.typeVehicle;
      const licensePlate = req.body.licensePlate;
      const findEmployee = await EmployeeModel.findOne({ _id: employeeId });
      const findLicensePlate = await VehicleModel.findOne({
        licensePlate: licensePlate,
      });
      if (findLicensePlate) {
        res.status(405).json({
          status: 405,
          success: false,
          message: "This vehicle is already in the data",
        });
      } else {
        VehicleModel.create({
          employeeId: employeeId,
          nameEmployee: findEmployee.fullName,
          typeVehicle: typeVehicle,
          licensePlate: licensePlate,
        });
        res.status(200).json({
          status: 200,
          success: true,
          message: "Create successfully",
        });
      }
    } catch (error) {
      res.status(500).json({
        status: 200,
        success: false,
        message: error,
      });
    }
  }

  async getVehicle(req, res) {
    try {
      const page_size = parseInt(req.query.PageSize) || 500;
      const page_number = parseInt(req.query.PageIndex) || 1;
      const getDatas = await ManageNotificationModel.find({
        employeeId: req.params.employeeId,
      }).select("-__v");

      let getData = [];
      for (let item of getDatas) {
        const itemPush = {
          ...item._doc,
          dateIn: item.dateIn ? formatDatePost(new Date(item.dateIn)) : null,
          dateOut: item.dateOut ? formatDatePost(new Date(item.dateOut)) : null,
        };
        getData.push(itemPush);
      }

      res.status(200).json({
        success: true,
        status_code: 200,
        meta: {
          total: getData.length,
          page_size: page_size,
          page_number: page_number,
        },
        data: Pagination(getData.reverse(), page_size, page_number),
      });
    } catch (error) {
      res.status(500).json({
        success: false,
        status_code: 500,
        message: error,
      });
    }
  } // search theo bien so xe

  async getThongTinXe(req, res) {
    const licensePlate = req.query.licensePlate;
    try {
      const licensePlateCheck = await VehicleModel.findOne({
        licensePlate: licensePlate,
      }).select("-__v");

      const managePlateCheck = await ManageNotificationModel.find({
        licensePlate: licensePlate,
      });

      console.log(licensePlateCheck, managePlateCheck);

      if (licensePlateCheck) {
        if (managePlateCheck.length > 0) {
          res.status(200).json({
            status: 200,
            success: true,
            message: {
              ...licensePlateCheck._doc,
              dateIn: formatDatePost(managePlateCheck[managePlateCheck.length - 1]?.dateIn) || null,
              dateOut: managePlateCheck[managePlateCheck.length - 1].dateOut ? formatDatePost(managePlateCheck[managePlateCheck.length - 1].dateOut) : null,
            },
          });
        } else {
          console.log(licensePlateCheck);

          res.status(200).json({
            status: 200,
            success: true,
            message: {
              ...licensePlateCheck._doc,
              dateIn: null,
              dateOut: null,
            },
          });
        }
      } else {
        res.status(400).json({
          status: 400,
          success: false,
          message: "Không tìm thấy xe này",
        });
      }
    } catch (err) {
      res.status(500).json({
        status: 500,
        success: false,
        message: err,
      });
    }
  }

  async getXeCuaEmployee(req, res) {
    const employeeId = req.params.employeeId;
    try {
      const licensePlateCheck = await VehicleModel.find({
        employeeId: employeeId,
      });
      if (licensePlateCheck) {
        res.status(200).json({
          status: 200,
          success: true,
          message: licensePlateCheck,
        });
      } else {
        res.status(400).json({
          status: 400,
          success: false,
          message: "Không tìm thấy dân cư này",
        });
      }
    } catch (err) {
      res.status(500).json({
        status: 500,
        success: false,
        message: err,
      });
    }
  }

  async checkInVehicle(req, res) {
    try {
      const date = new Date();
      const randomID = generateId(50);
      const licensePlate = req.body.licensePlate;
      const findVehicle = await VehicleModel.findOne({
        licensePlate: licensePlate,
      });
      if (findVehicle) {
        if (!findVehicle.qr) {
          qrcode.toDataURL(randomID, (err, src) => {
            if (err) res.send("Something went wrong!!");
            findVehicle.codeIn = randomID;
            findVehicle.qr = src;
            findVehicle.save();
          });
          const checkCapacity = await CapacityModel.findOne({ typeVehicle: findVehicle.typeVehicle });
          if (checkCapacity.nowVehicle === checkCapacity.capacity) {
            res.status(402).json({
              success: 402,
              status_code: false,
              message: "Bãi xe đã full chỗ",
            });
          } else {
            checkCapacity.nowVehicle = checkCapacity.nowVehicle + 1;
            checkCapacity.save();
            ManageNotificationModel.create({
              employeeId: findVehicle.employeeId,
              nameEmployee: findVehicle.nameEmployee,
              typeVehicle: findVehicle.typeVehicle,
              licensePlate: findVehicle.licensePlate,
              codeIn: randomID,
              dateIn: date,
            });
            res.status(200).json({
              success: true,
              status_code: 200,
              message: "Checkin Thành Công",
            });
          }
        } else {
          res.status(402).json({
            success: 402,
            status_code: false,
            message: "Xe này đã có trong bãi xe",
          });
        }
      } else {
        res.status(402).json({
          success: false,
          status_code: 402,
          message: "Không tìm thấy biển số xe",
        });
      }
    } catch (error) {
      res.status(500).json({
        success: false,
        status_code: 500,
        message: error,
      });
    }
  }

  async checkOutVehicle(req, res) {
    try {
      const date = new Date();
      const qrCode = req.body.qrCode;
      const findVehicle = await VehicleModel.findOne({ codeIn: qrCode });
      const findManage = await ManageNotificationModel.findOne({
        codeIn: qrCode,
      });

      if (!findVehicle) {
        res.status(400).json({
          success: false,
          status_code: 400,
          message: "Mã QR không hợp lệ",
        });
      } else {
        const checkCapacity = await CapacityModel.findOne({ typeVehicle: findVehicle.typeVehicle });
        checkCapacity.nowVehicle = checkCapacity.nowVehicle - 1;
        checkCapacity.save();
        findVehicle.codeIn = null;
        findVehicle.qr = null;
        findManage.codeIn = null;
        console.log("abc");
        findManage.dateOut = date;
        findVehicle.save();
        findManage.save();
        res.status(200).json({
          success: true,
          status_code: 200,
          message: "Check out thành công",
        });
      }
    } catch (error) {
      res.status(500).json({
        success: false,
        status_code: 500,
        message: "Server Lỗi",
      });
    }
  }

  async getVehicleWidthTypeVehicle(req, res) {
    const keySearchName = req.query.keySearchName;
    const typeVehicle = req.query.idTypeVehicle;
    const page_size = parseInt(req.query.PageSize) || 500;
    const page_number = parseInt(req.query.PageIndex) || 1;
    let getVehicle;
    if (!keySearchName) {
      getVehicle = await VehicleModel.find({ typeVehicle: typeVehicle });
    } else {
      getVehicle = await VehicleModel.find({
        typeVehicle: typeVehicle,
        nameEmployee: { $regex: keySearchName },
      });
    }
    res.status(200).json({
      success: true,
      status_code: 200,
      meta: {
        total: getVehicle.length,
        page_size: page_size,
        page_number: page_number,
      },
      data: Pagination(getVehicle.reverse(), page_size, page_number),
    });
  }

  async checkOutForAdmin(req, res) {
    try {
      const idManage = req.params.idManage;
      const licensePlate = req.params.licensePlate;

      const findManage = await ManageNotificationModel.findOne({ _id: idManage });
      const findVehicle = await VehicleModel.findOne({ licensePlate: licensePlate });
      console.log("a");
      console.log(findManage);
      console.log(findVehicle);
      if (findManage.dateOut) {
        console.log("b");
        res.status(400).json({
          success: false,
          status_code: 400,
          message: "Xe này không hợp lệ",
        });
      } else {
        console.log("c");
        findVehicle.codeIn = null;
        findVehicle.qr = null;
        findManage.codeIn = null;
        findManage.dateOut = formatDatePost(date);
        findVehicle.save();
        findManage.save();
        res.status(200).json({
          success: true,
          status_code: 200,
          message: "Check out thành công",
        });
      }
    } catch (error) {
      res.status(500).json({
        success: false,
        status_code: 500,
        message: "Server Lỗi",
      });
    }
  }

  async deleteVehicle(req, res) {
    const idVehicle = req.query.idVehicle;
    try {
      const checkVehicle = await VehicleModel.findOne({ _id: idVehicle });
      if (checkVehicle) {
        if (checkVehicle.qr) {
          res.status(400).json({
            success: false,
            status_code: 400,
            message: "Xe này vẫn còn đang ở trong bãi, hãy check out trước",
          });
        } else {
          VehicleModel.find({ _id: idVehicle }).remove().exec();
          res.status(200).json({
            success: true,
            status_code: 200,
            message: "Xóa thành công",
          });
        }
      } else {
        res.status(400).json({
          success: false,
          status_code: 400,
          message: "Không tìm thấy xe trong hệ thống",
        });
      }
    } catch (error) {
      res.status(500).json({
        success: false,
        status_code: 500,
        message: "Sever Error",
      });
    }
  }

  async getVehicleInParking(req, res) {
    const keySearchName = req.query.keySearchName;
    const typeVehicle = req.query.typeVehicle;
    const page_size = parseInt(req.query.PageSize) || 500;
    const page_number = parseInt(req.query.PageIndex) || 1;
    let getVehicle = [];
    if (!typeVehicle) {
      if (!keySearchName) {
        getVehicle = await VehicleModel.find({});
      } else {
        getVehicle = await VehicleModel.find({
          nameEmployee: { $regex: keySearchName },
        });
      }
    } else {
      if (!keySearchName) {
        getVehicle = await VehicleModel.find({ typeVehicle: typeVehicle });
      } else {
        getVehicle = await VehicleModel.find({
          typeVehicle: typeVehicle,
          nameEmployee: { $regex: keySearchName },
        });
      }
    }

    const getVehicleInParking = [];
    for (let item of getVehicle) {
      if (item.qr) {
        getVehicleInParking.push(item);
      }
    }
    res.status(200).json({
      success: true,
      status_code: 200,
      meta: {
        total: getVehicleInParking.length,
        page_size: page_size,
        page_number: page_number,
      },
      data: Pagination(getVehicleInParking.reverse(), page_size, page_number),
    });
  }

  async getDanhSachXe(req, res) {
    const gettAllXe = await VehicleModel.find({});
    res.status(200).json({
      success: true,
      status_code: 200,
      data: gettAllXe
    })
  }
}

module.exports = new VehicleController();
