const ManageNotificationModel = require("../models/ManageNotification");
const Pagination = require("../utils/Pagination");
const {
  formatDatePost,
  formatDatePostShort,
} = require("../../app/utils/ParseTimeDay");
class ManageController {
  async getInForMation(req, res) {
    const keySearchLicensePlate = req.query.keySearchLicensePlate;
    const page_size = parseInt(req.query.PageSize) || 500;
    const page_number = parseInt(req.query.PageIndex) || 1;
    const getVehicles = await ManageNotificationModel.find({
      licensePlate: { $regex: keySearchLicensePlate || "" },
    });
    let getVehicle = [];
    for(let item of getVehicles){
      const itemPush = {
        ...item._doc,
        dateIn: formatDatePost((new Date(item.dateIn))),
        dateOut: item.dateOut ? formatDatePost((new Date(item.dateOut))) : null
      };
      getVehicle.push(itemPush);
    }
    res.status(200).json({
      success: true,
      status_code: 200,
      meta: {
        total: getVehicle.length,
        page_size: page_size,
        page_number: page_number,
      },
      data: Pagination(getVehicle.reverse(), page_size, page_number),
    });
  }

  async getInForMationInTime(req, res) {
    const keySearchLicensePlate = req.query.keySearchLicensePlate;
    const page_size = parseInt(req.query.PageSize) || 500;
    const page_number = parseInt(req.query.PageIndex) || 1;
    const getVehicles = await ManageNotificationModel.find({
      licensePlate: { $regex: keySearchLicensePlate || "" },
    }).select("-dateOut");

    let getVehicle = [];
    for(let item of getVehicles){
      const itemPush = {
        ...item._doc,
        dateIn: formatDatePost((new Date(item.dateIn))),
      };
      getVehicle.push(itemPush);
    }

    res.status(200).json({
      success: true,
      status_code: 200,
      meta: {
        total: getVehicle.length,
        page_size: page_size,
        page_number: page_number,
      },
      data: Pagination(getVehicle.reverse(), page_size, page_number),
    });
  }

  async getInForMationOutTime(req, res) {
    const keySearchLicensePlate = req.query.keySearchLicensePlate;
    const page_size = parseInt(req.query.PageSize) || 500;
    const page_number = parseInt(req.query.PageIndex) || 1;
    const getVehicles = await ManageNotificationModel.find({
      licensePlate: { $regex: keySearchLicensePlate || "" },
    }).select("-dateIn");
    const getVehicleItem = [];
    for(let item of getVehicles){
      if(item.dateOut){
        getVehicleItem.push(item);
      }
    }
    let getVehicle = [];
    for(let item of getVehicleItem){
      const itemPush = {
        ...item._doc,
        dateOut: formatDatePost((new Date(item.dateOut))),
      };
      getVehicle.push(itemPush);
    }
    res.status(200).json({
      success: true,
      status_code: 200,
      meta: {
        total: getVehicle.length,
        page_size: page_size,
        page_number: page_number,
      },
      data: Pagination((getVehicle.sort((a, b) => b.dateOut - a.dateOut)).reverse(), page_size, page_number),
    });
  }
  
}

module.exports = new ManageController();
