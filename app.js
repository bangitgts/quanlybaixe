const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const bodyParser = require("body-parser");
const qrcode = require("qrcode");

const {
  formatDatePost,
  formatDatePostShort,
} = require("./app/utils/ParseTimeDay");
const generateId= require("./app/utils/GenerateId");
const app = express();
// Parse application/x-www-form-urlencoded
// for parsing application/json
app.use(bodyParser.json());
// for parsing application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());
const route = require("./app/routes");
const db = require("./config/connectDb.js");
// for parsing multipart/form-data
// [USE] parse application/json
// [USE] Morgan
db.connect();
app.use(morgan("combined"));
// ip use
app.get("/api/", (req, res) => {
  res.json(generateId(50));
});
route(app);
app.get("/", function (req, res) {
  res.sendFile(__dirname + "/index.html");
});


app.post("/multiple-upload", (req, res, next) => {
  const idAccount = req.body.idAccount;
  next();
});

app.get("/time",(req,res,next)=>{
  const date = new Date();
  console.log(date);
  res.json(formatDatePost(date.toString()));
});

var server = app.listen(3002, function () {
  var host = server.address().address;
  var port = server.address().port;
  console.log("Example app listening at http://%s:%s", host, port);
});
